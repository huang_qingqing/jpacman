package nl.tudelft.jpacman.npc.ghost;
import com.google.common.collect.Lists;
import nl.tudelft.jpacman.board.BoardFactory;
import nl.tudelft.jpacman.board.Direction;
import nl.tudelft.jpacman.level.Level;
import nl.tudelft.jpacman.level.LevelFactory;
import nl.tudelft.jpacman.level.Player;
import nl.tudelft.jpacman.level.PlayerFactory;
import nl.tudelft.jpacman.points.PointCalculator;
import nl.tudelft.jpacman.sprite.PacManSprites;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;


public class InkyTest {

    private GhostMapParser ghostMapParser;

    @BeforeEach
    void setup() {

        PacManSprites sprites = new PacManSprites();

        GhostFactory ghostFactory = new GhostFactory(sprites);

        BoardFactory boardFactory = new BoardFactory(sprites);
        LevelFactory levelFactory = new LevelFactory(sprites,
            ghostFactory, mock(PointCalculator.class));

        ghostMapParser = new GhostMapParser(levelFactory, boardFactory, ghostFactory);
    }
    /**
     *地图中inky对象和目的地之间无可达路径.
     */
    @Test
    void test1() {
        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "###########",
                "#I # B    #",
                "######### #",
                "#########P#"));

        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());

        Player player = new PlayerFactory(new PacManSprites()).createPacMan();

        player.setDirection(Direction.valueOf("WEST"));

        level.registerPlayer(player);

        Optional<Direction> opt = inky.nextAiMove();

        assertThat(opt.isPresent()).isFalse();
    }

    /**
     *地图中没有Blinky对象.
     */
    @Test
    void test2() {

        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "###########",
                "#I        #",
                "######### #",
                "#########P#"));


        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());
        Blinky blinky = Navigation.findUnitInBoard(Blinky.class, level.getBoard());

        Player player = new PlayerFactory(new PacManSprites()).createPacMan();

        player.setDirection(Direction.valueOf("WEST"));

        level.registerPlayer(player);

        assertThat(blinky).isNull();

        Optional<Direction> opt = inky.nextAiMove();

        assertThat(opt.isPresent()).isFalse();
    }

    /**
     *地图中没有Player对象.
     */
    @Test
    void test3() {

        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "###########",
                "#I  B     #",
                "######### #",
                "######### #"));


        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());

        assertThat(level.isAnyPlayerAlive()).isFalse();

        Optional<Direction> opt = inky.nextAiMove();

        assertThat(opt.isPresent()).isFalse();
    }

    /**
     *地图中blinky对象和player对象前2格空格之间无可达路径.
     */
    @Test
    void test4() {

        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "###########",
                "#I  B   # #",
                "######### #",
                "#########P#"));

        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());

        Player player = new PlayerFactory(new PacManSprites()).createPacMan();

        player.setDirection(Direction.valueOf("WEST"));

        level.registerPlayer(player);

        Optional<Direction> opt = inky.nextAiMove();

        assertThat(opt.isPresent()).isFalse();
    }



    /**
     *地图中inky对象和目的地之间有可达路径.
     */
    @Test
    void test5() {
        //地图解析，并产生对应种类npc
        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "###########",
                "#I   B    #",
                "######### #",
                "#########P#"));

        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());

        Player player = new PlayerFactory(new PacManSprites()).createPacMan();

        player.setDirection(Direction.valueOf("WEST"));

        level.registerPlayer(player);

        Optional<Direction> opt = inky.nextAiMove();

        assertThat(opt.get()).isEqualTo(Direction.valueOf("EAST"));
    }
}
