package nl.tudelft.jpacman.npc.ghost;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.google.common.collect.Lists;
import nl.tudelft.jpacman.board.BoardFactory;
import nl.tudelft.jpacman.board.Direction;
import nl.tudelft.jpacman.level.Level;
import nl.tudelft.jpacman.level.LevelFactory;
import nl.tudelft.jpacman.level.Player;
import nl.tudelft.jpacman.level.PlayerFactory;
import nl.tudelft.jpacman.points.PointCalculator;
import nl.tudelft.jpacman.sprite.PacManSprites;


public class ClydeTest {

    private GhostMapParser ghostMapParser;
    @SuppressWarnings("checkstyle:WhitespaceAfter")
    @SuppressWarnings({"checkstyle:WhitespaceAfter", "checkstyle:LineLength"})

    @BeforeEach
    void setup() {
        PacManSprites sprites = new PacManSprites();
        GhostFactory ghostFactory = new GhostFactory(sprites);
        BoardFactory boardFactory = new BoardFactory(sprites);
        LevelFactory levelFactory = new LevelFactory(sprites,ghostFactory, mock(PointCalculator.class));
        ghostMapParser = new GhostMapParser(levelFactory, boardFactory, ghostFactory);
    }

    /**
     *Clyde和Player之间间隔小于8个方块.
     */
    @Test
    void test1() {

        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "###########",
                "#C       P#",
                "###########"));


        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());

        Player player = new PlayerFactory(new PacManSprites()).createPacMan();

        player.setDirection(Direction.valueOf("WEST"));

        level.registerPlayer(player);

        Optional<Direction> opt = clyde.nextAiMove();

        assertThat(opt.get()).isEqualTo(Direction.valueOf("WEST"));
    }

    /**
     *Clyde和Player之间间隔大于8个方块.
     */
    @Test
    void test2() {
        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "##############",
                "#C          P#",
                "##############"));


        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());

        Player player = new PlayerFactory(new PacManSprites()).createPacMan();

        player.setDirection(Direction.valueOf("WEST"));

        level.registerPlayer(player);

        Optional<Direction> opt = clyde.nextAiMove();

        assertThat(opt.get()).isEqualTo(Direction.valueOf("EAST"));
    }
    /**
     *Player对象不存在.
     */
    @Test
    void test3() {
        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "##############",
                "C####         ",
                "##############"));

        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());

        assertThat(level.isAnyPlayerAlive()).isFalse();

        Optional<Direction> opt = clyde.nextAiMove();

        assertThat(opt.isPresent()).isFalse();
    }

    /**
     *Clyde和Player之间不可达.
     */
    @Test
    void test4() {

        Level level = ghostMapParser.parseMap(
            Lists.newArrayList(
                "#############P",
                "#C #########P#",
                "##############"));


        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());

        Player player = new PlayerFactory(new PacManSprites()).createPacMan();

        player.setDirection(Direction.valueOf("WEST"));

        level.registerPlayer(player);

        Optional<Direction> opt = clyde.nextAiMove();

        assertThat(opt.isPresent()).isFalse();
    }
}
